//
// Copyright (c) 2020 Michael Mestnik
// Licensed under the LGPL, see LICENSE in the project root.
//

use std::cell::RefCell;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use std::rc::Rc;

use wasm_bindgen::closure::Closure;
use wasm_bindgen::JsValue;

use web_sys::{Element as ElementInner, Event, EventTarget};

const EXPANDO_PREFIX: &str = "abc-data-";

pub trait Handler {
    fn send(&self, event: Event);
}

pub type Handlers = Rc<RefCell<Vec<Vec<Rc<dyn Handler>>>>>;

thread_local! {
    static NATIVE_HANDLERS: RefCell<HashMap<(NativeEventTarget, String), Result<NativeHandler, JsValue>>> = Default::default();
}

#[derive(PartialEq, Eq, Hash)]
enum NativeEventTarget {
    Element(Element),
    Window,
    Document,
}

#[derive(PartialEq, Eq)]
struct Element(ElementInner);

impl Hash for Element {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.id().hash(state);
    }
}

#[derive(Clone)]
struct NativeHandler {
    handlers: Handlers,
    type_: String,
    closure: Rc<Closure<dyn FnMut(JsValue)>>,
}

pub(crate) fn drop_for_element(element: ElementInner, type_: String) {
    NATIVE_HANDLERS.with(|this| {
        this.borrow_mut()
            .remove(&(NativeEventTarget::Element(Element(element)), type_));
    })
}

pub(crate) fn get_for_element(element: ElementInner, type_: String) -> Result<Handlers, JsValue> {
    NATIVE_HANDLERS.with(|this| -> Result<Handlers, JsValue> {
        let this = this
            .borrow_mut()
            .entry((
                NativeEventTarget::Element(Element(element.clone())),
                type_.clone(),
            ))
            .or_insert_with(|| -> Result<NativeHandler, JsValue> {
                use wasm_bindgen::JsCast;
                let handlers: Handlers = Default::default();
                let handlers_weak = Rc::downgrade(&handlers);
                let closure = Closure::wrap(Box::new(move |event: JsValue| {
                    let event: Event = event.into();
                    handlers_weak.upgrade().map(|handlers| {
                        handlers.borrow().iter().for_each(|handlers| {
                            handlers.iter().for_each(|handler| {
                                handler.send(event.clone());
                            })
                        })
                    });
                }) as Box<dyn FnMut(JsValue)>);

                let target: &EventTarget = element.as_ref();
                target
                    .add_event_listener_with_callback(&type_, closure.as_ref().unchecked_ref())?;
                Ok(NativeHandler {
                    handlers,
                    type_,
                    closure: Rc::new(closure),
                })
            })
            .clone()?;
        Ok(this.handlers.clone())
    })
}
