//
// Copyright (c) 2020 Michael Mestnik
// Licensed under the LGPL, see LICENSE in the project root.
//

use std::cell::RefCell;
use std::rc::{Rc, Weak};

use wasm_bindgen::prelude::*;

use web_sys::{Document, HtmlElement, Window};

thread_local! {
    static WINDOW: Window = web_sys::window().expect("should have a window in this context");
    static DOCUMENT: Document = WINDOW.with(|w|w.document().expect("window should have a document"));
}

thread_local! {
  static INVALID_INLINE_TARGETS: Vec<&'static str> = vec![
"area", "base", "basefont", "br", "col", "frame", "hr", "img", "input", "isindex", "link", "meta", "param", "embed", "source", "wbr", "track",
"colgroup", "option", "table", "tbody", "tfoot", "thead", "tr", "th", "td", "script", "noscript", "style", "textarea", "video", "audio", "iframe", "object", "menu",
];}

mod dom_util {
    fn unique_id(prefix: Option<&str>) -> String {
        use std::sync::atomic::{AtomicU32, Ordering::Relaxed};
        static COUNTER: AtomicU32 = AtomicU32::new(0);
        let counter = COUNTER.fetch_add(1, Relaxed);
        format!("{}_{}", prefix.unwrap_or("abc"), counter)
    }
}

mod events;

#[derive(Clone)]
pub struct Shortcut {
    pub ctrl: bool,
    pub shift: bool,
    pub meta: bool,
    pub alt: bool,
    pub key_code: u32,
    pub char_code: char,
    pub subpatterns: Vec<Shortcut>,
    pub description: String,
}

struct ContextInner {
    editors: Vec<Editor>,
}

impl ContextInner {
    fn new() -> Self {
        Self { editors: vec![] }
    }

    fn add_editor(
        &mut self,
        element: HtmlElement,
        plugins: &[&str],
        context: ContextWeak,
    ) -> Editor {
        let editor = Editor::new(element, plugins, context);
        self.editors.push(editor.clone());
        editor
    }

    pub fn add_shortcut(&mut self, shortcut: &Shortcut) {
        for editor in self.editors.iter_mut() {
            editor.shortcut_push(shortcut.clone());
        }
    }
}

#[derive(Clone)]
pub struct ContextWeak(Weak<RefCell<ContextInner>>);

#[derive(Clone)]
pub struct Context(Rc<RefCell<ContextInner>>);

impl Context {
    pub fn new() -> Self {
        Self(Rc::new(RefCell::new(ContextInner::new())))
    }

    fn new_weak(&self) -> ContextWeak {
        ContextWeak(Rc::downgrade(&self.0))
    }

    pub fn add_editor(&self, element: HtmlElement, plugins: &[&str]) -> Editor {
        let context = self.new_weak();
        self.0.borrow_mut().add_editor(element, plugins, context)
    }

    pub fn editor_push(&self, editor: Editor) {
        self.0.borrow_mut().editors.push(editor);
    }

    pub fn add_shortcut(&self, shortcut: &Shortcut) {
        self.0.borrow_mut().add_shortcut(shortcut);
    }
}

struct EditorInner {
    context: ContextWeak,
    element: HtmlElement,
    shortcuts: Vec<(Shortcut, Option<Box<dyn FnMut()>>)>,
}

impl EditorInner {
    fn new(element: HtmlElement, plugins: &[&str], context: ContextWeak) -> Self {
        Self {
            element,
            context,
            shortcuts: vec![],
        }
    }
}

#[derive(Clone)]
pub struct Editor(Rc<RefCell<EditorInner>>);

impl Editor {
    pub fn new(element: HtmlElement, plugins: &[&str], context: ContextWeak) -> Self {
        let inner = EditorInner::new(element.clone(), plugins, context);
        Self(Rc::new(RefCell::new(inner)))
    }

    pub fn shortcut_push(&mut self, shortcut: Shortcut) {
        self.0.borrow_mut().shortcuts.push((shortcut, None));
    }
}
